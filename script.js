

/*FUnciones operar */

function operar(){


    string_resultado = realizar_cuenta(document.getElementById("pantalla").value);

	document.getElementById("pantalla").value=string_resultado;
}


function realizar_cuenta(string){
	if(string.length==0)
		return string;

	if (existen_patrones_invalidos(string))
		return "Valor invalido";

	string = realizar_cuenta_mayor_denominacion(string);
	
	console.log(string);

    string =  cuenta_menor_denominacion(string);
	
	console.log(string);

	return (string);

}


function realizar_cuenta_mayor_denominacion(string){

	let primer_numero="";
	let segundo_numero="";
	let comienzo_operacion=0;
	let termino_operacion=0;

	for(let i =0;i<string.length;i++){
		if(es_mayor_rango(string[i])){

			for(let j=i-1;j>=0;j--){
				
				comienzo_operacion=j
				
				if(es_operador(string[j])){
					if(string[j]=='-')
						primer_numero+=string[j];

					break;
				}
			 primer_numero+=string[j];
			}

			primer_numero = primer_numero.split("").reverse().join("");

			for(let k =i+1;k<=string.length;k++){
				termino_operacion=k;
				
				if(termino_operacion==string.length)
					break;

				if(es_operador(string[k])&&(string[k]!='-'||es_operador(string[k-1])==false)){
					break;
				}
				segundo_numero+=string[k];
			}


			console.log(string+"---------"+primer_numero+"---------"+segundo_numero+"------------"+comienzo_operacion+"---"+termino_operacion);
			string = (modificacion_string(comienzo_operacion,termino_operacion,realizar_operacion(parseFloat(primer_numero),parseFloat(segundo_numero),string[i]),string));
			
			
			primer_numero="";
			segundo_numero="";
			i=-1;
			}

	}
	return string;
}

function modificacion_string(comienzo_operacion,termino_operacion,resultado,string){
	let nuevo_string="";
	let result_parse=resultado+"";

	
	if(es_operador(string[comienzo_operacion])){
		if(string[comienzo_operacion]=='-')
			nuevo_string = string.substring(0,comienzo_operacion)+resultado+string.substring(termino_operacion,string.length);
		else
			nuevo_string = string.substring(0,comienzo_operacion+1)+resultado+string.substring(termino_operacion,string.length);	
	
			
		}
	else	
		 nuevo_string = string.substring(0,comienzo_operacion)+resultado+string.substring(termino_operacion,string.length);

	return nuevo_string;

}


function cuenta_menor_denominacion(string){
	let arr_suma=[];
	let numero_a_sumar="";
	let resultado=0;
	for(let i=0;i<string.length;i++){

		if((es_operador(string[i])&&(numero_a_sumar.length>0))){
			
			arr_suma.push(parseInt(numero_a_sumar));
			
			numero_a_sumar="";
			
			if(es_operador(string[i+1])==false)
				numero_a_sumar+=string[i];
			
		}
		else
			numero_a_sumar+=string[i];
	
		}
	
	arr_suma.push(parseInt(numero_a_sumar));

	console.log(arr_suma);
	

	for(let j =0;j<arr_suma.length;j++)
		resultado+=arr_suma[j];

	return resultado;
}

function buscar_ultimo_numero(index,string){
	
	 if (string[index]=='+'||string[index]=='-'||string[index]=='x'||string[index]=='/'){
			//buscar el sig index con operador
			for(let i=index+1;i<string.length;i++){
				
				if(string[i]=='+'||string[i]=='-'||string[i]=='x'||string[i]=='/')
					return i-1;
				
			}
		
		return -2;
	 
	}


	 return -1;

}



function realizar_operacion(numero1,numero2,operador){

	if(operador=='x')
		return numero1*numero2;
	
	if(operador=='/')
		return numero1/numero2;
	
	if(operador=='+')
		return numero1+numero2;
	
	if(operador=='-')
		return numero1-numero2

}



function es_operador(caracter){
	return caracter=='+'||caracter=='x'||caracter=='/'||caracter=='-'
}

function es_mayor_rango(caracter){

	return caracter=='x'||caracter=='/';

}

function es_num(caracter){

	if(caracter=='0')
		return true;
	if(caracter=='1')
		return true;
	if(caracter=='2')
		return true;
	if(caracter=='3')
		return true;
	if(caracter=='4')
		return true;
	if(caracter=='5')
		return true;
	if(caracter=='6')
		return true;
	if(caracter=='7')
		return true;
	if(caracter=='8')
		return true;
	if(caracter=='9')
		return true;

	return false
	}


function verificar_input(input_pantalla){

	let regex = /x+/;

	if (input_pantalla.match(regex).length!=null){

		console.log(input_pantalla.match(regex));

		console.log("el input no es valido");
	
	}
	else
		console.log("el input  es valido");
	


}

/*Funciones pantalla*/

function reiniciar_input(){

    document.getElementById("pantalla").value = "";
    
}

function borrar_ultimo_caracter(){

	let indice_ultimo_caracter = document.getElementById("pantalla").value.length-1;
	let string_nuevo = document.getElementById("pantalla").value.substring(0,indice_ultimo_caracter);
	document.getElementById("pantalla").value = string_nuevo;

}

function concatenar_caracter(elem){

   
	let concatenar = document.getElementById("pantalla").value+=elem;
   
    document.getElementById("pantalla").value = concatenar;
   
}



/*Busca patrones invalidos con expresiones regulares*/
function existen_patrones_invalidos(string){

	let buscar_patron = (/\W{2}|x{2}/i)
	let buscar_letras= (/[^x|\d|\W]/i)

	return (buscar_patron.test(string) || es_operador(string[string.length-1])||buscar_letras.test(string));

	
}
